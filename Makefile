

TAR	= ./server \
	  ./client \
	  ./readfile \
	  ./writefile 

$(TAR)= Wall


server:server.go
	go build -o server server.go

client:client.go
	go build -o client client.go

writefile:writefile.go
	go build -o writefile writefile.go

readfile:readfile.go
	go build -o readfile readfile.go


