package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func main()  {

	//打开文件
	opfile,err := os.Open("a.txt")
	if err != nil{
		fmt.Println("打开文件时 : ",err)
		return
	}
	read_str := bufio.NewReader(opfile)
	for{
		str,err := read_str.ReadString('\n')
		if err == io.EOF{
			continue
		}
		if err != nil{
			fmt.Println("读取时发生 : ",err)
			return
		}

		fmt.Println(str)
	}



}
