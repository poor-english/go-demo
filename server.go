package main

import (
	"fmt"
	"github.com/godbus/dbus"
	"log"
	"net"
	"os"
	"sync"
	"time"
)

var mu sync.Mutex
var mu_file sync.Mutex

func main()  {
	//启动事件
	for{
		tcpaccept()
	}


	//主线程睡眠
	time.Sleep(time.Hour * 100)
}

//处理log
func setlog(str string)  {
	//打开文件
	openfile,err := os.OpenFile("log",os.O_WRONLY | os.O_CREATE | os.O_APPEND,0664)
	defer openfile.Close()
	if err != nil{
		fmt.Println("打开或创建log文件时 : ",err)
		return
	}
	//设置log的格式
	log.SetFlags(log.Llongfile | log.Ltime | log.Ldate)
	//设置log的输入
	mu_file.Lock()
	defer mu_file.Unlock()
	log.SetOutput(openfile)
	//拼接语句,写入
	log.Println(str)

}

//处理发送结果
func sendstr(conn net.Conn)  {

		go addsig(conn)
		go delsig(conn)


}

//建立tcp连接
func tcpaccept()  {
	//建立监听套结字
	socket_liten,err := net.Listen("tcp","127.0.0.1:9999")
	if err != nil{
		fmt.Println("建立监听socket时 : ",err)
		
		return
	}
	//建立连接
	for{
		socket_conn ,err := socket_liten.Accept()
		if err != nil{
			fmt.Println("连接socket时 : ",err)
			return
		}
		//发送数据
		go sendstr(socket_conn)
	}
}

//检测删除信号
func delsig(conn net.Conn)  {
	//连接dbus
	dbus_conn,err := dbus.SessionBus()
	if err != nil{
		fmt.Println("连接信号时发生 : ",err)
		return
	}
	//添加信号
	err = dbus_conn.BusObject().AddMatchSignal("com.deepin.dde.daemon.Dock","EntryRemoved",dbus.WithMatchObjectPath("/com/deepin/dde/daemon/Dock")).Err
	if err != nil{
		fmt.Println("添加信号时  :  ",err)
		return
	}
	sigch := make(chan *dbus.Signal,10)
	dbus_conn.Signal(sigch)

	for  {
		select {
		//检测信号
		case sig:=<- sigch:
			if sig.Name == "com.deepin.dde.daemon.Dock.EntryRemoved" &&
				sig.Path == "/com/deepin/dde/daemon/Dock"{
				//获取信息
				var sig_string string
				err = dbus.Store(sig.Body,&sig_string)
				//拼接信息，发送给客户端
				now := time.Now() //获取当前时间
				send_str := fmt.Sprintf("在 [%s],删除组件，捕捉的信号是 : %s\n",now.Format("2006-01-02 15:04:05"),sig_string)
				fmt.Println("发送消息预览 : ",send_str)
				mu.Lock()
				_,err = conn.Write([]byte(send_str))
				mu.Unlock()
				if err != nil{
					fmt.Println("add发送数据时  : ",err)
					return
				}
			}
		default:

		}
		time.Sleep(time.Second * 10)
	}

	time.Sleep(time.Hour * 100)

	//删除信号
	err = dbus_conn.BusObject().RemoveMatchSignal("com.deepin.dde.daemon.Dock","EntryRemoved",dbus.WithMatchObjectPath("/com/deepin/dde/daemon/Dock")).Err
	if err != nil{
		fmt.Println("删除信号时 : ",err)
		return
	}
	dbus_conn.RemoveSignal(sigch)
}

//检测添加信号
func addsig(conn net.Conn)  {
	//连接dbus
	dbus_conn,err := dbus.SessionBus()
	if err != nil{
		fmt.Println("连接dbus失败......")
		return
	}
	//获取对象
	//dbus_obj := dbus_conn.Object("com.deepin.dde.daemon.Dock","/com/deepin/dde/daemon/Dock")

	//添加信号
	err = dbus_conn.BusObject().AddMatchSignal("com.deepin.dde.daemon.Dock","EntryAdded",dbus.WithMatchObjectPath("/com/deepin/dde/daemon/Dock")).Err
	if err != nil{
		fmt.Println("添加信号时 :  ",err)
		return
	}
	sigch := make(chan *dbus.Signal,10)
	dbus_conn.Signal(sigch)

	for true {
		select {
		//检测信号
		case sig:=<- sigch:
			if sig.Name == "com.deepin.dde.daemon.Dock.EntryAdded" &&
				sig.Path == "/com/deepin/dde/daemon/Dock"{
				//获取信息
				var num int32
				var str_obj dbus.Object
				err = dbus.Store(sig.Body,&str_obj,&num)
				fmt.Println("捕捉的信号 : ",str_obj,num)
				
				//拼接信息发送
				now := time.Now() //获取时间
				send_str := fmt.Sprintf("在[%s]时间 添加组件，捕捉的信号是  %s:%d\n",now.Format("2006-01-02 15:04:05"),str_obj,num)
				//发送信息
				mu.Lock()
				_,err = conn.Write([]byte(send_str))
				mu.Unlock()
				if err != nil{
					fmt.Println("del发送数据时  : ",err)
					return
				}
			}
		default:
			fmt.Println("暂时没有事件")
		}
		time.Sleep(time.Second * 3)
	}

	time.Sleep(time.Hour * 100)
	//删除信号
	err = dbus_conn.BusObject().RemoveMatchSignal("com.deepin.dde.daemon.Dock","EntryAdded",dbus.WithMatchObjectPath("/com/deepin/dde/daemon/Dock")).Err
	if err != nil{
		fmt.Println("删除信号时 :  ",err)
		return
	}
	dbus_conn.RemoveSignal(sigch)
}
