package main

import (
	"fmt"
	"github.com/godbus/dbus"
	"os"
	"sync"
	"time"
)

var mu sync.Mutex

//写文件操作
func main()  {

	fmt.Println("OK")

	go addsig()
	go delsig()

	time.Sleep(time.Hour + 10)
}

func writefile(str string)  {
	//打开文件

	openfile,err := os.OpenFile("a.txt",os.O_CREATE | os.O_APPEND | os.O_WRONLY,0644)
	defer openfile.Close()
	if err != nil{
		fmt.Println("写入文件时发生  :  ",err)
		return
	}
	mu.Lock()
	defer mu.Unlock()
	fmt.Fprintf(openfile,str)
	openfile.Close()
}

//检测删除信号
func delsig()  {
	//连接dbus
	dbus_conn,err := dbus.SessionBus()
	if err != nil{
		fmt.Println("连接信号时发生 : ",err)
		return
	}
	//添加信号
	err = dbus_conn.BusObject().AddMatchSignal("com.deepin.dde.daemon.Dock","EntryRemoved",dbus.WithMatchObjectPath("/com/deepin/dde/daemon/Dock")).Err
	if err != nil{
		fmt.Println("添加信号时  :  ",err)
		return
	}
	sigch := make(chan *dbus.Signal,10)
	dbus_conn.Signal(sigch)

	for  {
		select {
		//检测信号
		case sig:=<- sigch:
			if sig.Name == "com.deepin.dde.daemon.Dock.EntryRemoved" &&
				sig.Path == "/com/deepin/dde/daemon/Dock"{
				//获取信息
				var sig_string string
				err = dbus.Store(sig.Body,&sig_string)
				//拼接信息，发送给客户端
				now := time.Now() //获取当前时间
				send_str := fmt.Sprintf("在 [%s],发生了删除组件事件，捕捉的信号是 : %s\n",now.Format("2006-01-02 15:04:05"),sig_string)
				fmt.Println("发送消息预览 : ",send_str)
				//写文件
				writefile(send_str)
			}
		default:

		}
		time.Sleep(time.Second * 3)
	}

	time.Sleep(time.Hour * 100)

	//删除信号
	err = dbus_conn.BusObject().RemoveMatchSignal("com.deepin.dde.daemon.Dock","EntryRemoved",dbus.WithMatchObjectPath("/com/deepin/dde/daemon/Dock")).Err
	if err != nil{
		fmt.Println("删除信号时 : ",err)
		return
	}
	dbus_conn.RemoveSignal(sigch)
}

//检测添加信号
func addsig()  {
	//连接dbus
	dbus_conn,err := dbus.SessionBus()
	if err != nil{
		fmt.Println("连接dbus失败......")
		return
	}
	//获取对象
	//dbus_obj := dbus_conn.Object("com.deepin.dde.daemon.Dock","/com/deepin/dde/daemon/Dock")

	//添加信号
	err = dbus_conn.BusObject().AddMatchSignal("com.deepin.dde.daemon.Dock","EntryAdded",dbus.WithMatchObjectPath("/com/deepin/dde/daemon/Dock")).Err
	if err != nil{
		fmt.Println("添加信号时 :  ",err)
		return
	}
	sigch := make(chan *dbus.Signal,10)
	dbus_conn.Signal(sigch)

	for true {
		select {
		//检测信号
		case sig:=<- sigch:
			if sig.Name == "com.deepin.dde.daemon.Dock.EntryAdded" &&
				sig.Path == "/com/deepin/dde/daemon/Dock"{
				//获取信息
				var num int32
				var str_obj dbus.Object
				err = dbus.Store(sig.Body,&str_obj,&num)
				fmt.Println("捕捉的信号 : ",str_obj,num)

				//拼接信息发送
				now := time.Now() //获取时间
				send_str := fmt.Sprintf("在[%s]时间 发送了添加组件事件，捕捉的信号是  %s:%d\n",now.Format("2006-01-02 15:04:05"),str_obj,num)
				writefile(send_str)
			}
		default:
			writefile("没有事件\n")
		}
		time.Sleep(time.Second * 3)
	}

	time.Sleep(time.Hour * 100)
	//删除信号
	err = dbus_conn.BusObject().RemoveMatchSignal("com.deepin.dde.daemon.Dock","EntryAdded",dbus.WithMatchObjectPath("/com/deepin/dde/daemon/Dock")).Err
	if err != nil{
		fmt.Println("删除信号时 :  ",err)
		return
	}
	dbus_conn.RemoveSignal(sigch)
}

