package main

import (
	"fmt"
	"io"
	"net"
	"time"
)

func main()  {
	fmt.Println("OK")

	//建立连接
	socket_conn,err := net.Dial("tcp","0.0.0.0:9999")
	if err != nil{
		fmt.Println("client连接时 : ",err)
		return
	}
	//关闭连接
	defer socket_conn.Close()
	//接收数据
	var buf [128]byte
	str := "已经收到数据"
	for{
		n,err := socket_conn.Read(buf[:])
		if err == io.EOF{
			fmt.Println("client 接收数据完成")
			return
		}
		if err != nil{
			fmt.Println("client 接收数据时 : ",err)
			continue
		}
		fmt.Println("接收来自服务端的消息 :    ",string(buf[:n]))
		//回应消息
		socket_conn.Write([]byte(str))
		time.Sleep(time.Second * 10)//睡眠10秒
	}

}
